// import Text from 'cheerio/node_modules/domhandler/lib/index.js'
import mmacro from './macro-more.esm.js'
export let $ = null
let t
export const domTool = t = {
    setCheerio(x) { $ = x },
    setCheerioModule(ch) {
        $ = ch.load(`
<!DOCTYPE html><html><head><meta charset="UTF-8"></head>
<body></body></html>`)
    },
    nodeSym: Symbol('custom-node'),
    isNode: x => x && (x instanceof $ || x.s == domTool.nodeSym),
    createNode: x => $(`<${x}>`),
    setAttribute(e, k, v) {
        e.attr(k, v)
    },
    append(p, c) {
        if (c.s == domTool.nodeSym) c.appendTo(p)
        else p.append(c)
    },
    createText: s => false || {
        type: 'text-node',
        x: s,
        s: domTool.nodeSym,
        appendTo: domTool.textNodeAppendTo,
        toHtml: domTool.textNodeToHtml
    },
    textNodeToHtml() {
        return $('<span>').text(this.x).html()
    },
    textNodeAppendTo(p) {
        const $e = $('<span>').text(this.x)
        p.append($e.contents())
    },
    createFragment: () => false || {
        type: 'fragment-node',
        s: domTool.nodeSym,
        children: [],
        append(x) { this.children.push(x) },
        appendTo: domTool.fragmentAppendTo,
        toHtml: domTool.fragmentToHtml
    },
    fragmentToHtml() {
        return this.children
            .map(e => domTool.toHtml(e))
            .join('')
    },
    fragmentAppendTo(p) {
        this.children.forEach(x => domTool.append(p, x))
    },
    toHtml(x) {
        if (this.nodeSym == x.s && x.toHtml) return x.toHtml()
        else return $.html(x)
    }
}

export function createRootLikeNode(x) {
    return $(x).clone().empty()
}
function macroRootLikeNode(l) {
    const name = l[0]
    l[0] = createRootLikeNode(name)
}
for (const k of 'html head body'.split(' ')) {
    domTool[`macro:${k}`] = macroRootLikeNode
}
domTool['macro:doctype'] = l => {
    const $dt = $($('html')[0].prev).clone()
    $dt.append = () => {}
    $dt.attr = () => {}
    l[0] = $dt
}
Object.assign(domTool, mmacro.domTool)
domTool['macro:html-raw'] = domTool['macro:html-raw-cheerio']
