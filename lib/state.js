/** example
  patchTsjson(tsj)

  const nm = State.of('unnamed') // name
  const c = State.of(0) // counter
  const step = State.of(1) // increment

const div = tsj.html `
[div
  [input [:oninput ${e => nm.val = e.target.value}
          :value ${nm.val}]]
  [input [:type button :value "counter (${c}+${step})"
          :onclick ${e => c.val+=step.val}]]
  [button [:onclick ${()=>[c.val,step.val]=[0,1]}] reset]
  [input [:value ${step} :oninput ${e => step.val=Number(e.target.value)}]]

  [p "hey ${nm}, ${c}th visit "]
  [p ${c.derive(() => tsj.html
    `[strong [:style [font-size ${c.val*2+18}px]]
       "foo in ${c}*2+18 = ${c.val*2+18} px"]`)}]]`
*/

class State {
    constructor(x) {
        this._val = this.valNew = x
        this.valOld = null
        const emt = this.emt = new EventTarget()
        emt.state = this
    }
    toString() {
        return String(this.val)
    }
    valueOf() {
        return this.val
    }
    static of(x) {
        return new this(x)
    }
    static domStateRefMap = new WeakMap()
    // s will not get reclaim before e
    static domSatetRefAdd(e, s) {
        const wm = this.domStateRefMap
        let l = wm.get(e)
        if (!l) {
            l = []
            wm.set(e, l)
        }
        l.push(s)
    }
    get val() {
        return this._val
    }
    set val(x) {
        this.valOld = this._val
        this._val = this.valNew = x
        const e = new Event('change')
        e.state = this
        this.emt.dispatchEvent(e)
    }
    onWeak(...obj) {
        const f = obj.pop()
        const f2 = bindWeak(...obj, f)
        const f3 = (...arg) => {
            const r = f2(...arg)
            if (r == bindWeak.reclaim) this.off(f3)
        }
        this.on(f3)
        return f2
    }
    on(n, f) {
        if (typeof n == 'function') [n, f] = ['change', n]
        this.emt.addEventListener(n, f)
    }
    off(n, f) {
        if (typeof n == 'function') [n, f] = ['change', n]
        this.emt.removeEventListener(n, f)
    }
    update() {
        this.val = this.fn(...this.upstream)
    }
    derive(f) {
        return this.constructor.derive(this, f)
    }
    static derive(...arg) {
        let f = arg[arg.length - 1]
        if (typeof f == 'function') arg.pop()
        else f = () => {}
        const c = new State(f(...arg))
        c.fn = f
        c.upstream = arg
        for (const s of arg) {
            s.onWeak(c, c => c.update())
        }
        return c
    }
    static isState(x) {
        return x && x instanceof this
    }
    static strf(ss, ...vs) {
        const l = []
        for (var i=0; i<vs.length; i++) {
            let v = vs[i]
            if (this.isState(v)) v = v.val
            l.push(ss[i], v)
        }
        l.push(ss[i])
        return l.join('')
    }
    static deriveStrf(ss, ...vs) {
        return this.derive(
            ...vs.filter(x => this.isState(x)),
            () => this.strf(ss, ...vs)
        )
    }
    static hubCreate() {
        const s = new this()
        return class extends this {
            constructor(...a) {
                super(...a)
                const cls = this.constructor
                this.hub = cls.hub
                this.on('change', () => this.hub.val = this)
            }
            static hub = s
            static derive(...arg) {
                const s = super.derive(...arg)
                const hub = this.hub
                hub.on('change', () => s.upstream.indexOf(hub.val) == -1 && s.update())
                return s
            }
        }
    }
}

function patchTsjson(tsj) {
    tsj.State = State
    tsj.Future.resolveMap.unshift((type, xs) => {
        if (xs.every(x => !tsj.State.isState(x))) return null
        return tsj.tag('variable', State.deriveStrf(
            xs.filter((x, i) => i % 2 == 0),
            ...xs.filter((x, i) => i % 2 == 1)
        ))
    })
    const t = tsj.domTool
    tsj.domTool = {
        __proto__: t,
        // support <style> macro
        isState: x => tsj.State.isState(x),
        isNode(x) {
            if (super.isNode(x)) return true
            return this.isState(x) && super.isNode(x.val)
        },
        isAttributeDict(x) {
            return super.isAttributeDict(x) && !this.isState(x)
        },
        append(p, c) {
            let e = c
            if (this.isState(c)) {
                e = c.val
                let r = new WeakRef(e)
                const f = () => {
                    const e = r.deref()
                    if (!e) return c.off(f)
                    const e2 = c.val
                    e.replaceWith(e2)
                    r = new WeakRef(e2)
                }
                c.on(f)
                tsj.State.domSatetRefAdd(p, c)
            }
            return super.append(p, e)
        },
        setAttribute(e, k, v) {
            const isState = this.isState
            if (isState(v)) {
                const vs = v
                vs.onWeak(e, e => this.setAttribute(e, k, vs.val))
                v = v.val
                tsj.State.domSatetRefAdd(e, vs)
            }
            return super.setAttribute(e, k, v)
        },
        createText(x) {
            if (!this.isState(x)) return super.createText(x)
            const e = super.createText(x.val)
            x.onWeak(e, e => e.data = x.val)
            tsj.State.domSatetRefAdd(e, x)
            return e
        }
    }
}

bindWeak.reclaim = Symbol('bindWeak.object_reclaimed')
export function bindWeak(...arg) {
    const f = arg.pop()
    const argr = arg.map(o => new WeakRef(o))
    return (...arg2) => {
        const arg = argr.map(r => r.deref())
        if (arg.some(x => x === undefined)) return weakWrap.reclaim
        return f(...arg, ...arg2)
    }
}
