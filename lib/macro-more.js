const macro = {
    'html-raw-cheerio'(list) {
        const $div = this.createNode('div')
        $div.html(list.slice(1).join(''))
        list.splice(0, list.length, $div.contents())
    },
    'html-raw-browser'(list) {
        const div = this.createNode('div')
        div.innerHTML = list.slice(1).join('')
        const frag = this.createFragment()
        while (div.firstChild) this.append(frag, div.firstChild)
        list.splice(0, list.length, frag)
    },
    script(a) {
        let i=1
        if (this.isAttributeDict(a[i])) i++
        for (true; i<a.length; i++) {
            if (typeof a[i] == 'function') {
                const fn = a[i]
                if (fn.constructor == Function) {
                    a[i] = macro.getFunctionBody(fn)
                }
                else a[i] = `void (${fn.toString()})();`
            }
        }
    },
    getFunctionBody(f) {
        let code = f.toString()
        if (/^function /.test(code)) {
            code = code.replace(/^function.*?\)/, '')
        }
        else if (code.charAt(0) == '(') {
            code = code.replace(/^\(.*?=>\s*/, '')
        }
        else code = code.replace(/^.*?=>\s*/, '')
        code = code.trim()
        if (code.charAt(0) == '{') code = code.slice(1, -1)
        return code
    },
    do(l) {
        l[0] = this.createFragment()
        l.splice(1).forEach((f, i) => {
            const r = f()
            if (r != undefined) l.push(r)
        })
    },
    macro(l) {
        l[0] = this.createFragment()
        while (l.length >= 3) {
            const [n, f] = l.splice(1, 2)
            this[`macro:${n}`] = f
        }
    },
    '::call'(e, k, v, ctx) {
        if (typeof v == 'string') v = v.trim().split(/\s+/)
        if (!Array.isArray(v)) v = [v]
        for (let x of v) {
            let f = x
            if (typeof f != 'function') f = this[`macro::${x}`]
            x.call(this, e, x, null, ctx)
        }
    },
    '::shadow-root'(e, k, v, ctx) {
        const [opt, ...tree] = v
        e.attachShadow(opt)
        tsjson.html(ctx) `[${e.shadowRoot} @${tree}]`
    },
    'content-set'(l) {
        l.shift()
        l[0].textContent = ''
    },
    text(l) {
        l.shift()
        let b = ['(', ')']
        const f = x => {
            if (!Array.isArray(x)) return String(x)
            return b[0] + x.map(y => f(y)).join(' ') + b[1]
        }
        const s = f(l).slice(1, -1)
        l.splice(0, l.length, this.createText(s))
    },
    css(l) {
        l[0] = 'style'
        this['macro:style'](l)
        l[0] = 'css'
    },
    '!---'(l) {
        l.splice(0, l.length, this.createFragment())
    },
    remove(l) {
        l[0] = 'div'
    }
}

const domTool = Object.fromEntries(
    Object.entries(macro).map(cons => {
        if (!/[A-Z]/.test(cons[0])) cons[0] = 'macro:' + cons[0]
        return cons
    })
)

domTool['macro:style::syntax'] = (c, k, v) => `"${v}"`
domTool['macro:style:::var'] = function (c, k, v) {
    k = v[0]
    v = v.slice(1)
    const w = x => `var(${x})`
    if (v.length == 1) v[0] = w(v[0])
    else v = v.map(x => Array.isArray(x) ? w(x[0]) : x)
    const m = `macro:style::${k}`
    if (this[m]) {
        const r = this[m](c, k, v)
        // if another macro return a key explicity
        if (Array.isArray(r)) return r
        else v = r
    }
    return [k].concat(v)
}

// comment
domTool['macro:style::///'] = (l, k, v) => null
domTool['macro:style:://'] = (l, k, v) => {
    let c = v
    if (Array.isArray(c)) c = c.join(' ')
    l.push(`/* ${c} */`)
}

// raw "--foo: bar; /* comment */"
// -> --foo: bar; /* comment */
domTool['macro:style:::raw'] = (css, k, v) => void css.push(v)

// bin/tsj-html.js '[div [:style [display block // "comment 2" :raw "/* foo */" color white]] foo]  [!--- this is comment ---]'
// <div style="display: block; /* comment 2 */ /* foo */ color: white;">foo</div>

Object.assign(exports, {domTool, macro})
