const ex = exports

const OPEN    = {type: 'open'},
      CLOSE   = {type: 'close'};

const SPACE   = /^[ \r\n\t]+/,
      NUMBER  = /^-?\d+(?:\.\d+)?/,
      ATOM    = /^[^\(\)\[\]'"\r\n\t ]+/,
      STRING  = /^((["'])(?:\\.|\\\n|[^\\])*?\2)/;

ex.token = {OPEN, CLOSE, SPACE, NUMBER, ATOM, STRING}

// helpers
const ok = (x, m) => { if (!x) throw new Error(m || 'assertion fail') }

ex.decodeStringMap = '\n  0 \0 r \r n \n t \t \\ \\ \' \' " " $ $ ` `'
    .split(' ')
ex.decodeString = (str) => str.replace(/\\(x..|u\w{4}|u\{.{1,6}\}|[od]\d{1,3}|.|\n)/g, seq => {
    const map = ex.decodeStringMap
    const code = seq.charAt(1)
    const i = map.indexOf(code)
    if (i % 2 == 0) return map[i+1]
    const arg = seq.slice(2)
    if (arg.length == 0) return seq
    let n
    if (seq.slice(1,3) == 'u{') {
        n = Number('0x' + arg.slice(1,-1))
    }
    else if (code == 'x' || code == 'u') {
        n = Number(`0x${arg}`)
    }
    else if (code == 'o') n = Number(`0o${arg}`)
    else if (code == 'd') n = Number(arg)
    if (n >= 0) return String.fromCodePoint(n)

    return seq
})


function SexpStream(options) {

    options = options || {};
    options.objectMode = true;

    this._txNumber = options.translateNumber || parseFloat;
    this._txSymbol = options.translateSymbol || function(s) { return s; };
    this._txString = options.translateString || function(s) { return s; };
    
    this.init()
    this._remain = '';
    this.result = []
}

const pt = SexpStream.prototype
pt.init = function () {
    this.queueVar = []
    this.slotVar = []
}
SexpStream.prototype._transform = function(chunk, encoding, done) {

    this._remain += chunk;
    const [bleft, bright] = ex.bracket
    const {OPEN, CLOSE, SPACE, NUMBER, ATOM, STRING} = ex.token
    var match, len;
    const qv = this.queueVar
    while (this._remain.length) {
        if (this._remain[0] === bleft) {
            this.endVar(qv)
            this.push(OPEN);
            len = 1;
        } else if (this._remain[0] === bright) {
            this.endVar(qv)
            this.push(CLOSE);
            len = 1;
        } else if ((match = SPACE.exec(this._remain))) {
            this.endVar(qv)
            len = match[0].length;
        } else if (match = /^[\[\]()]/.exec(this._remain)) {
            const c = match[0]
            throw new Error(`use bracket '${c}' while \
bracket is '${ex.bracket.join('')}'`)
        } else {
            if ((match = ATOM.exec(this._remain))) {
                if (match[0].length < this._remain.length) {
                    let x = null
                    if (qv.length == 0) {
                        const nm = NUMBER.exec(match[0])
                        if (nm && nm[0] == match[0]) {
                            const n = this._txNumber(match[0])
                            if (!Number.isNaN(n.value)) x = n
                        }
                    }
                    if (!x && match[0] == '@' && qv.length == 1) {
                        if (qv[0].pos == 1) {
                            this.push(this._txSymbol('@'))
                            x = qv.shift()
                        }
                    }
                    if (!x && qv.length == 0) x = ex.parsePrimitive(match[0])
                    if (!x) {
                        x = this.joinVar(match[0], qv)
                        x = x.smartUnbox()
                    }
                    this.push(x);
                    len = match[0].length;
                } else {
                    break;
                }
            } else if ((match = STRING.exec(this._remain))) {
                let s = this.joinVar(match[0], qv)
                s.decodeString()
                s.futureType = 'string'
                s = s.smartUnbox()
                this.push(s);
                len = match[0].length;
            } else {
                break;
            }
        }
        this._remain = this._remain.slice(len);
    }

    done();

};

SexpStream.prototype._flush = function(cb) {
    cb(this._remain.length ? new Error("unexpected EOF") : null);
};

SexpStream.prototype.push = function (x) {
    this.result.push(x)
}
SexpStream.prototype.write = function (s) {
    this._transform(s, 'utf8', ()=>{})
}
pt.endVar = function (queueVar = this.queueVar) {
    if (queueVar.length == 0) return false
    const v = queueVar.splice(0)
    if (v.length == 1) this.push(v[0])
    else {
        ok(v.every(x => x.pos == 0))
        const fu = this.joinVar('', v)
        this.push(fu)
    }
}
pt.joinVar = function (s, vs = this.queueVar) {
    const result = []
    const slen = s.length
    let start = 0
    for (const v of vs) {
        const pos = v.pos
        ok(pos <= slen)
        result.push(s.slice(start, pos))
        start = pos
    }
    result.push(s.slice(start))
    vs.splice(0)
    return new ex.Future(result)
}
pt.toTemplateHash = function (s, x) {
    const xid = []
    const sid = []
    let pos = 0
    let i=0
    for (true; i<x.length; i++) {
        sid.push(s[i])
        pos += s[i].length
        xid.push(pos)
    }
    sid.push(s[i])
    return `${x.length}:${xid.join(':')}:${sid.join('')}`
}

pt.writeVar = function (x) {
    const v = ex.tag('variable-future', null)
    v.pos = this._remain.length
    this.queueVar.push(v)
    this.slotVar.push(x)
}
pt.fromTagArray = function (s, x) {
    let i
    for (i=0; i<x.length; i++) {
        this.write(s[i])
        this.writeVar(x[i])
    }
    this.write(s[i] + ' ') // end the last token
}
pt.toJson = function (xs = this.slotVar) {
    const tag = ex.tag
    const stack = new Stack([tag('undef', null)])
    xs = new Queue(xs)
    const {OPEN, CLOSE} = ex.token
    for (const x of this.result) {
        const origin = x
        // const x = o.value
        // const type = o.type
        switch (x) {
        case OPEN: {
            const sub = tag('undef', null)
            stack.push(sub)
        }
            break

        case CLOSE: {
            const sub = stack.pop()
            const parent = stack.top
            if (sub.type == 'undef') {
                sub.type = 'list'
                sub.value = []
            }
            if (parent.splice) {
                parent.splice = false
                this.boxAddSplice(parent, sub)
            }
            else this.boxAdd(parent, sub)
        }
            break

        default: {
            const parent = stack.top
            let x = origin
            if (x.type == 'variable-list-future') {
                x = x.resolve(xs.getN(x.length))
            }
            else if (x.type == 'variable-future') {
                x = tag('variable', xs.get())
            }
            if (parent.type == 'undef') {
                if (x.type == 'symbol' && ex.isColonPrefix(x.value)) {
                    parent.type = 'dict'
                    parent.value = {}
                    parent.key = null
                    if (x.value == ':') break
                }
                else {
                    parent.type = 'list'
                    parent.value = []
                }
            }
            if (x.type == 'symbol' && x.value == '@') {
                parent.splice = true
                break
            }
            if (parent.splice) {
                parent.splice = false
                this.boxAddSplice(parent, x)
            }
            else this.boxAdd(parent, x)
        }
        }
    }
    if (stack.top.type == 'undef') return []
    return stack.top.value
}

pt.boxAddSplice = function (box, sub) {
    const {tag} = ex
    const {boxAdd} = this
    if (sub.type == 'variable-list-future') {
        throw new Error('can not splice variable')
    }
    if (sub.type == 'variable') {
        if (ex.isDict(sub.value)) sub = tag('dict', sub.value)
        else if (Array.isArray(sub.value)) {
            sub = tag('list', sub.value)
        }
        else sub = tag('list', Array.from(sub.value))
    }
    const x = sub.value
    if (sub.type == 'dict') {
        for (const [k, v] of ex.loopDict(x)) {
            boxAdd(box, tag('string', k))
            boxAdd(box, tag('string', v))
        }
    }
    else x.forEach(x => boxAdd(box, tag('string', x)))
}
pt.boxAdd = function (box, x) {
    if (box.type == 'undef') {
        box.type = 'list'
        box.value = []
    }
    if (box.type == 'list') return box.value.push(x.value)
    if (box.type == 'dict') {
        if (box.key == null) {
            let key = x.value
            if (x.type == 'symbol' && ex.isColonPrefix(key)) {
                key = key.slice(1)
            }
            box.key = key
        }
        else {
            box.value[box.key] = x.value
            box.key = null
        }
    }
}
pt.tag = function (s, ...x) {
    return this.fromTagArray(s.raw || s, x)
}
ex.enableCache = function () {
    this.cache = new Map()
    this.j = function () {
        const ss = new SexpStream(ex.optionJsexp)
        const [s, ...x] = arguments
        const hash = ss.toTemplateHash(s, x)

        const c = ex.cache
        if (c.has(hash)) {
            ss.result = c.get(hash).result
            return ss.toJson(x)
        }

        ss.tag(...arguments)
        c.set(hash, ss)
        if (ss._remain.length > 0) throw new Error('unexpected end of data')
        return ss.toJson()
    }
}
ex.j = function () {
    const ss = new SexpStream(ex.optionJsexp)
    if (typeof arguments[0] == 'string') {
        ss.write(arguments[0] + ' ')
    }
    else ss.tag(...arguments)
    if (ss._remain.length > 0) {
        throw new Error(`unexpected end of data: ${ss._remain}`)
    }
    return ss.toJson()
}
ex.isColonPrefix = function (key) {
    return typeof key == 'string' && key.charAt(0) == ':'
}
ex.optionJsexp = {
    translateNumber(n) {
        return ex.tag('number', parseFloat(n))
    },
    translateSymbol(o) {
        return ex.tag('symbol', o)
    },
    translateString(s) {
        return ex.tag('string', s)
    }
}
ex.tag = (type, value) => false || {type, value}
ex.isArray = x => Array.isArray(x)
ex.isDict = x => x && typeof x == 'object' &&
    (x instanceof Map || !(Symbol.iterator in x))
ex.loopDict = x => x instanceof Map ? x.entries() : Object.entries(x)
ex.html = function (...args) {
    let ctx = args[0]
    if (args.length == 1 && this.isDict(ctx)) {
        return (...args) => this.html(ctx, ...args)
    }
    const t = this.domTool
    if (this.isDict(ctx)) args.shift()
    else {
        if (!t.context || Object.keys(t.context).length > 0) t.context = {}
        ctx = t.context
    }
    const a = this.j(...args)
    if (a.length == 1) return this.json2html(a[0], ctx)
    a.unshift(t.createFragment())
    return this.json2html(a, ctx)
}
ex.domTool = {
    isNode: x => x instanceof Node,
    isAttributeDict(x) {
        return ex.isDict(x) && !this.isNode(x)
    },
    createNode: name => document.createElement(name),
    setAttribute(e, k, v) {
        if (k == 'class') k = 'className'
        if (k in e) e[k] = v
        else e.setAttribute(k, v)
    },
    append(p, c) {p.appendChild(c)},
    createText: (t) => document.createTextNode(t),
    createFragment: () => document.createDocumentFragment(),
    context: {}
}
ex.domTool['macro:style'] = function (l) {
    let i = 1
    if (ex.domTool.isAttributeDict(l[i])) i++
    l.splice(i, 0, '\n')
    i++
    for (true; i<l.length; i++) {
        const x = l[i]
        if (!Array.isArray(x)) continue
        // if (x[0].charAt(0) == '@') ;
        let sel = x[0]
        if (Array.isArray(sel)) sel = sel.join(' ')
        const prop = x.slice(1)
        let s = `${sel} {\n`
        s += '  ' + this.formatCssLine(...prop).join('\n  ')
        s += '\n}\n'
        l[i] = s
    }
}
Object.assign(ex.domTool, {
    'macro:::id'(node, k, v, ctx) {ctx[v] = node},
    'macro:::call'(node, k, v) {v(node)},
    // macro:style:x preserve for @rule or sth
    'macro:style::font-family'(css, k, l) {
        if (!Array.isArray(l)) return l
        return l.map(f => `"${f}"`).join(', ')
    },
    formatCssLine(...args) {
        const t = this
        const isa = ex.isArray
        const css = []
        for (let i=0; i<args.length; i+=2) {
            let k = args[i]
            let v = args[i+1]
            const m = `macro:style::${k}`
            if (t[m]) b: {
                v = t[m](css, k, v)
                if (!isa(v)) break b
                k = v[0]
                v = v.slice(1)
            }
            if (v == null) continue
            if (isa(v)) v = v.join(' ')
            css.push(`${k}: ${v};`)
        }
        return css
    },
    'macro::style'(node, k, v) {
        if (!ex.isArray(v)) return v
        return this.formatCssLine(...v).join(' ')
    },
    'macro::class'(node, k, v) {
        if (ex.isArray(v)) v = v.join(' ')
        return v
    }
})

ex.isFunction = x => typeof x == 'function'
ex.json2html = function json2html(jsexp, ctx) {
    const domTool = ex.domTool
    const t = domTool
    const {isFunction} = ex
    if (t.isNode(jsexp)) return jsexp
    if (!ex.isArray(jsexp)) {
        return t.createText(jsexp)
    }
    const j = new Queue(jsexp)
    const nodeName = j.peek()
    if (typeof nodeName == 'string') {
        const m = `macro:${nodeName}`
        if (domTool[m]) domTool[m](j.array, ctx)
    }
    let node = j.get()
    if (!t.isNode(node)) node = t.createNode(node)
    const d = j.peek()
    if (t.isAttributeDict(d)) {
        j.get()
        for (const [k, v] of ex.loopDict(d)) {
            const m = 'macro::' + k
            if (domTool[m]) {
                const v2 = domTool[m](node, k, v, ctx)
                if (v2 !== undefined) t.setAttribute(node, k, v2)
            }
            else t.setAttribute(node, k, v)
        }
    }
    for (const child of j) t.append(node, json2html(child, ctx))
    return node
}
ex.jtable = function (...args) {
    return this.uncompress(this.j(...args))
}
ex.uncompress = function (jo) {
    const header = jo[0]
    const l = []
    for (let i=1; i<jo.length; true) {
        const o = {}
        for (const k of header) o[k] = jo[i++]
        l.push(o)
    }
    return l
}
ex.parsePrimitive = s => {
    const ss = 'true false null undefined NaN'.split(' ')
    const vs = [true, false, null, undefined, NaN]
    const i = ss.indexOf(s)
    if (i == -1) return null
    return ex.tag('primitive', vs[i])
}
ex.Future = class {
    constructor() {
        this.init(...arguments)
    }
    init(array) {
        this.array = array
        this.length = array.length - 1
        this.type = 'variable-list-future'
        this.futureType = 'symbol'
    }
    decodeString() {
        const a = this.array
        const ds = ex.decodeString
        for (let i=0; i<a.length; i++) a[i] = ds(a[i])
        a[0] = a[0].slice(1)
        const last = a.length-1
        a[last] = a[last].slice(0, -1)
    }
    smartUnbox() {
        if (this.length > 0) return this
        return ex.tag(this.futureType, this.array[0])
    }
    static resolveMap = [(type, xs) => ex.tag(type, xs.join(''))]
    resolve(xs) {
        const a2 = this.array.slice()
        const result = []
        let i
        for (i=0; i<xs.length; i++) result.push(a2[i], xs[i])
        result.push(a2[i])
        const type = this.futureType
        const l = this.constructor.resolveMap
        for (const f of l) {
            const r = f(type, result)
            if (r) return r
        }
    }
}
// TODO: move to ex.token.bracket and update docs
ex.bracket = ['(', ')']
ex.bind = function (x, ...args) {
    if (typeof x != 'function') x = this[x]
    return x.bind(this, ...args)
}
Object.assign(ex, {SexpStream})

class Base {
    constructor() {
        return this.init(...arguments)
    }
}

class Queue extends Base {
    init(a = []) {
        this.array = a
        this.i = 0
    }
    get '0'() {
        return this.peek()
    }
    get next() {
        return this.peek()
    }
    peek() {
        const a = this.array
        if (this.i >= a.length) return undefined
        return a[this.i]
    }
    get() {
        const a = this.array
        return a[this.i++]
    }
    getN(n) {
        const a = this.array
        const start = this.i
        this.i += n
        return a.slice(start, this.i)
    }
    push() {
        this.array.push(...arguments)
    }
    get length() {
        return this.array.length
    }
}
Queue.prototype[Symbol.iterator] = function *() {
    const l = this.length
    const a = this.array
    for (let i=this.i; i<l; i++) yield a[i]
}
class Stack extends Base {
    init(array = []) {
        this.array = array
    }
    get '0'() {return this.top}
    get top() {
        const a = this.array
        return a[a.length-1]
    }
    push() {
        return this.array.push(...arguments)
    }
    pop(n = 1) {
        const a = this.array
        if (n == 1) return a.pop()
        const l = a.length
        return a.splice(l-n, n)
    }
    empty() {
        return this.array.splice(0)
    }
    get count() {
        return this.array.length
    }
}

Object.assign(ex, {Queue, Stack, Base})
