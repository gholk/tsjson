import {TestClasser, assertShort} from '@gholk/tt-logger/test-class.js'
import tsjson from '../index.esm.js'
import assert from 'assert'

export {TestClasser, tsjson}
const {deq, ok} = assertShort
const fail = assert.fail

export function main() {

const j = tsjson.j
class TCJ extends TestClasser {
    defineDeqList(...args) {
        while (args.length > 0) {
            const [name, a, b] = args.splice(0, 3)
            this[name] = () => deq(a, b)
        }
    }
    'bracket change'() {
        tsjson.bracket = '[ ]'.split(' ')
        deq(
            j `a [b []] c 12['']`,
            ['a', ['b', []], 'c', 12, ['']]
        )
        tsjson.bracket = '( )'.split(' ')
        deq(
            j `a (b ()) c 12('')`,
            ['a', ['b', []], 'c', 12, ['']]
        )
    }
    'symbol can be key'() {
        deq(
            j `: ${Symbol.for('x')} x`,
            {[Symbol.for('x')]: 'x'}
        )
        const o = Symbol('x no public')
        deq(j `: ${o} ${o}`, {[o]: o})
    }
    'splice iteratable object as array'() {
        const o = {k: -1}
        o[Symbol.iterator] = function* () {yield *[1,2,3]}
        deq(
            j `1 @${o} 2`,
            [1, 1, 2, 3, 2]
        )

        const d = {k: 1}
        deq(j `1 @${d} end`,
            [1, 'k', 1, 'end'])
    }
    genMap() {
        const m = new Map()
        m.set('a', 1)
        m.set('key space', 2)
        m.set(3, 4)
        return [m, Object.fromEntries(m)]
    }
    'template basic'() {
        const ss = new tsjson.SexpStream(tsjson.optionJsexp)
        ss.tag `a 1 (:a 2 :b ${1+2} :c ${'zz'}) :nop`
        deq(ss.toJson([null, []]),
            ['a', 1, {a: 2, b: null, c: []}, ':nop'])
        deq(ss.toJson(), ['a', 1, {a: 2, b: 3, c: 'zz'}, ':nop'])
        deq(ss.toJson([null, []]),
            ['a', 1, {a: 2, b: null, c: []}, ':nop'])
    }
    'template splice'() {
        const ss = new tsjson.SexpStream(tsjson.optionJsexp)
        ss.tag `a ${1} @ (${'baz'} 3 foo)
                (:a 2 @(:b ${1+2} :b2 6) :c ${'zz'}) :nop`
        deq(ss.toJson([null, {k: 1}, NaN, 'ff']),
            ['a', null, {k:1}, 3, 'foo',
             {a: 2, b: NaN, b2: 6, c: 'ff'}, ':nop'])
        deq(ss.toJson(),
            ['a', 1, 'baz', 3, 'foo',
             {a: 2, b: 3, b2: 6, c: 'zz'}, ':nop'])
    }
    'splice map as dict'() {
        const [m, o] = this.genMap()
        ok(tsjson.isDict(m))
        const d = j `: k 1 @ ${m} k3 3`
        deq(d, {k:1, k3:3, ...o})
        // deq(d, {k:1, k3:3, a:1, 'key space':2, '3':4})
    }
    'parse does not modify slotVar'() {
        const ss = new tsjson.SexpStream(tsjson.optionJsexp)
        ss.tag `a ${1} @ (${'baz'} 3 foo)
                (:a 2 @(:b ${1+2} :b2 6) :c ${'zz'}) :nop`
        const v1 = ss.slotVar.slice()
        ss.toJson()
        deq(ss.slotVar, v1)
    }
}
const tcj = new TCJ()
tcj.defineDeqList(
    'plain list',
    j `1 "string" 3 symbol-x 0.5 -1 'single quote' -0.9 999999`,
    [1, "string", 3, 'symbol-x', 0.5, -1, 'single quote', -0.9, 999999],

    'plain list nest',
    j `1 2 (a b () 3) 0.7`,
    [1, 2, ['a', 'b', [], 3], 0.7],

    'plain dict',
    j `:foo bar :n 3 "space string" ok`,
    {foo: 'bar', n: 3, 'space string': 'ok'},

    'plain dict nest',
    j `:foo bar :n 3 :nest (:n2 2 :k (:v foo))`,
    {foo: 'bar', n: 3, nest: {n2: 2, k: {v: 'foo'}}},

    'plain dict list mix',
    j `:foo (a b (:k foo :n 18)) :k2 -1`,
    {foo: ['a', 'b', {k: 'foo', n: 18}], k2: -1},

    'colon prefix object',
    j `: key value k2 3`,
    {key: 'value', k2: 3},

    'colon optional after 1st key',
    j `:key value k2 3`,
    {key: 'value', k2: 3},

    'colon string is not dict',
    j `":key" 1 2`,
    [":key", 1, 2],

    'colon string no chomp colon',
    j `: ":key" value`,
    {':key': 'value'},

    'colon string first child not dict',
    j `":" ; ,`,
    ': ; ,'.split(' '),

    'colon double prefix',
    j `::colon_key ::colon-value ::col2 ::v2`,
    {':colon_key': '::colon-value', ':col2': '::v2'},

    'colon in variable concat symbol: is qualified key',
    j `${':'}key value`,
    {key: 'value'},

    'empty list',
    j ``,
    [],

    'empty dict',
    j `:`,
    {},

    'single list',
    j `()`,
    [[]],

    'single dict',
    j `(:)`,
    [{}],
    
    'undef box become list when first child not key',
    j `((:k v))`,
    [[{k: 'v'}]],

    'number key first prefix',
    j `: 1 2 3 4`,
    {'1': 2, '3': 4},

    'number key some prefix',
    j `:k value :2 2 :3 v3`,
    {k: 'value', '2': 2, '3': 'v3'},

    'variable can concat symbol',
    j `abc-${"d e f"} x1-${1+2} x1${2}`,
    ['abc-d e f', 'x1-3', 'x12'],

    'variable is keep if not concat',
    j `abc ${null} ${[1,2]} (:k ${{k2: 3}})`,
    ['abc', null, [1,2], {k: {k2: 3}}],

    'variable in string',
    j `1 "${123}" "a${'a'}b${'b'}${'c'}" "a\n${'b\\nc'}\\nd"`,
    [1, "123", "aabbc", 'a\nb\\nc\\nd'],

    'variable concat is symbol and embed in string no decode',
    j `1 (:${"foo"}x${"bar"}${"xx"} bar) (":ab${"foo"}" "${12 + '\\'}t")`,
    [1, {fooxbarxx: 'bar'}, [':abfoo', '12\\t']],

    'variable not break remain string',
    j `"foo${'inject" "succ'}bar" qq`,
    ['fooinject" "succbar', 'qq'],

    'variable concat variable is string concat',
    j `${'a'}${'b'}${12}`,
    ['ab12'],

    'number minus number: is not 2 number',
    j `1-2`,
    ['1-2'],

    'number concat variable produce symbol',
    j `1${2} -${3} ${4}${5}`,
    ['12', '-3', '45'],

    'json table uncompress',
    tsjson.uncompress(j `
      (id name key)
      1 'first name' first-name-key
      2 'easy name' easy-name`),
    j `(: id 1
          name 'first name'
          key first-name-key)
       (: id 2
          name 'easy name'
          key easy-name)`,

    'js literal primitive',
    j `true false null undefined NaN`,
    [true, false, null, undefined, NaN],
 
    'splice literal array',
    j `a b @ (1 2) 5 6`,
    ['a', 'b', 1, 2, 5, 6],

    'splice literal dict',
    j `: a b @ (:k1 v1 ::k2 v2 k2.5 v2.5) @ (k3 v3 :k4 v4)`,
    {a: 'b', k1: 'v1', ':k2': 'v2', 'k2.5': 'v2.5', k3: 'v3', ':k4': 'v4'},

    'splice variable array',
    j `a @ ${[1,2]} c`,
    ['a', 1, 2, 'c'],

    'splice variable dict',
    j `: k1 v1 @ ${{k2: 'v2', ':k3': 3}} k4 4`,
    {k1: 'v1', k2: 'v2', ':k3': 3, k4: 4},

    'splice nest',
    j `a b @ (1 2 @ (3 4)) 5 (:k6 6 @ (k7 7))`,
    ['a', 'b', 1, 2, 3, 4, 5, {k6: 6, k7: 7}],

    'splice string',
    j `a b @ ${"cdef"}`,
    'a b c d e f'.split(' '),

    'splice array in object',
    j `: a 1 b 2 @ (c 3 d 4) e 5`,
    {a: 1, b:2, c:3, d:4, e:5},

    'splice array in object un-order',
    j `: a 1 b @(2 c 3 d) 4`,
    {a:1, b:2, c:3, d:4},

    'splice postfix do nothing',
    j `0 ${[1,2]}@ 3`,
    [0, '1,2@', 3],

    'splice not work in at string',
    j `0 "@" (1 2)`,
    [0, "@", [1, 2]],

    'string escape quote in string',
    j `
"double: double [\"] single [\'] back quote \` back slash \\"
'single: double [\"] single [\'] back quote \` back slash \\'
'var: ${3} \${3}'`,
    [
        "double: double [\"] single ['] back quote ` back slash \\",
        "single: double [\"] single ['] back quote ` back slash \\",
        "var: 3 ${3}"
    ],

    'string plain input decode non-tag',
    j('1 2 (x 0) ${1+2}\nl2 \t "escape sequence \\n \\\\ \\t end"'),
    [1, 2, ['x', 0], '${1+2}', 'l2', 'escape sequence \n \\ \t end'],

    'string escape decode',
    j `"a \\ \n \\n \\\n \r \t"`,
    ['a \\ \n \\n \\\n \r \t'],

    'string binary decode',
    j `"hex \x20\x0a"
       "oct \o012 \o40 \o0 \o100"
       "dec \d10 \d9 \d122"
       "unicode \u0020 \u000a \u6211 \uFAIL"
       "unicode-long \u{00020} \u{000a} \u{1000f} \u{0D} \u{f} \u{0} \u{6211}"
       "null \0"`,
    ['hex \x20\x0a', 'oct \n \x20 \0 @', 'dec \n \t z',
     'unicode \u0020 \u000a \u6211 \\uFAIL',
     'unicode-long \u{00020} \u{000A} \u{1000F} \x0d \x0f \u0000 \u6211',
     'null \0'],

    'string escape new-line',
    j `escape-new-line "a\
b"`,
    ['escape-new-line', 'ab'],

    'string escape unrecognized is left as if',
    j `"\z \N \c \[ \] \{ \} \! \. \ "`,
    [String.raw `\z \N \c \[ \] \{ \} \! \. \ `],

    'string decode',
    tsjson.decodeString('r \\r n \\n t \\t \\_ \\\\ \' \\\' " \\" \\\\n'),
    'r \r n \n t \t \\_ \\ \' \' " " \\n'
)

tcj.runClass().exit()

}

import esMain from 'es-main'

if (esMain(import.meta)) main()
