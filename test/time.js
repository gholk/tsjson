import {main, tsjson, TestClasser} from './index.js'
import console from 'console'
import process from 'process'

TestClasser.prototype.exit = () => {}
TestClasser.prototype.log = () => {}

const runTimes = (f, count) => {
    console.time(`run-${count}`)
    for (let i=0; i<count; i++) f()
    console.timeEnd(`run-${count}`)
}

if ('CACHE' in process.env) tsjson.enableCache()

runTimes(main, Number(process.env.COUNT) || 1000)
