#!/usr/bin/env node
import cheerio from 'cheerio'
import tsjson from '../index.esm.js'
import {TestClasser, assertShort} from '@gholk/tt-logger/test-class.js'
import * as cht from '../lib/cheerio-dom-tool.js'

const {deq, eq, ok, nok} = assertShort
const neq = (a, b) => ok(a != b)

const domTool = tsjson.domTool
Object.assign(domTool, cht.domTool)
domTool.setCheerioModule(cheerio)

tsjson.bracket = '[]'

const h = (...args) => {
    const x = tsjson.html(...args)
    if (typeof x == 'function') {
        return (...args) => domTool.toHtml(x(...args))
    }
    return domTool.toHtml(x)
}

class TCJ extends TestClasser {
    defineEqTask(...args) {
        while (args.length > 0) {
            const [name, a, b] = args.splice(0, 3)
            this[name] = () => eq(a, b)
        }
    }
    'context default object'() {
        const $div = h `
            [div [span [::id span] "a span"]
                 [span [::id span2] span2]]`
        const {span, span2} = domTool.context
        deq(span.text(), 'a span')
        deq(span2.text(), 'span2')
    }
    'context with specify object'() {
        const ctx = {}
        const ctx0 = domTool.context
        const $div = h(ctx) `
            [div [span [::id span] "a span"]
                 [span [::id span2] span2]]`
        const {span, span2} = ctx
        deq(span.text(), 'a span')
        deq(span2.text(), 'span2')
        eq(ctx0, domTool.context)
    }
    'context get overwrite'() {
        const ctx0 = domTool.context = {x: null}
        h `[div [::id x]]`
        neq(ctx0, domTool.context)
    }
    'macro node ignore'() {
        domTool['macro:ignore'] = l => {
            l.splice(0)
            l.push(domTool.createFragment())
        }
        deq(h `[div [ignore [: this will] [not] output]]`, '<div></div>')
        domTool['macro:ignore'] = null
    }
    'macro attribute id'() {
        domTool['macro::id'] = (node, k, v) => {
            domTool.context[v] = node
            return v
        }
        const $div = tsjson.html `[div [:id a-div-id] empty]`
        eq(domTool.toHtml($div), '<div id="a-div-id">empty</div>')
        eq($div, domTool.context['a-div-id'])
        domTool['macro::id'] = null
    }
    'macro change node name'() {
        domTool['macro:my-div'] = function (list) {
            list[0] = 'div'
            if (!domTool.isAttributeDict(list[1])) list.splice(1, 0, {})
            const dict = list[1]
            dict['class'] = 'my-div'
        }
        eq(h `[div [my-div "my div with class"]]`,
           '<div><div class="my-div">my div with class</div></div>')
        const h2 = h `[div [my-div [:data-x xxx] "my div with class"]]`
        ok('<div><div data-x="xxx" class="my-div">my div with class</div></div>' == h2 ||
           '<div><div class="my-div" data-x="xxx">my div with class</div></div>' == h2)
        domTool['macro:my-div'] = null
    }
    'macro more'() {
        eq(h `[script ${z=>{alert("hello world!")}}]`,
           '<script>alert("hello world!")</script>')
        eq(h `[script [:type module]
                  "import tsj from './index.esm.js';"
                  ${z=> {var n = 0}}]`,
           '<script type="module">import tsj from \'./index.esm.js\';var n = 0</script>')
        eq(h `[script ${async z=>alert("hello world!")}]`,
           '<script>void (async z=>alert("hello world!"))();</script>')
        eq(h `[div [html-raw '<p class="foo">a foo para</p>']]`,
           '<div><p class="foo">a foo para</p></div>')
        eq(h `[html-raw "<!--comment data-->"]`, "<!--comment data-->")
        ok(cht.domTool.getFunctionBody)
        nok(cht.domTool['macro:getFunctionBody'])
    }
    'macro define macro'() {
        const t = domTool
        nok(t['macro:checkbox'])
        const html = h `
[macro checkbox ${l => {
    const t = tsjson.domTool
    l[0] = 'label'
    let d = {}
    if (t.isAttributeDict(l[1])) [d] = l.splice(1, 1)
    d.type = 'checkbox'
    const input = ['input', d]
    const [name] = l.splice(1, 1, input)
    d.name = name
    if (!l[2]) l[2] = name.replace(/[-_]/g, ' ')
}}]
[checkbox chk]
[checkbox [:class a-chk ::id achk] chk2 "some text " [strong "strong text"]]
`
        eq(html, `<label><input type="checkbox" name="chk">chk</label>\
<label><input class="a-chk" type="checkbox" name="chk2">some text \
<strong>strong text</strong></label>`)
        eq(t.context.achk.attr('name'), 'chk2')
        t['macro:checkbox'] = null
    }
    'macro call function'() {
        let freev = 1
        eq(h `[do ${()=>String(freev = 2)} ${()=>freev='a'}]`, '2a')
        eq(freev, 'a')
    }
    'parse does not modify jsexp object'() {
        const jo = tsjson.j `div [:id my-div :class bar] [span foo] bar`
        const jo0 = jo.slice()
        tsjson.json2html(jo, {})
        deq(jo, jo0)
    }
}
const tcj = new TCJ()

tcj.defineEqTask(
    'form full',
    h `
[form [:action form.js :method POST]
 [button [:id hello-btn :onclick 'alert("hello world")']
         'hello wrold']
 [input [:type reset]]
 [label 
  "username: " 
  [input [:name username
          :placeholder "please enter your name"]]]]`,
    '<form action="form.js" method="POST"><button id="hello-btn" onclick="alert(&quot;hello world&quot;)">hello wrold</button><input type="reset"><label>username: <input name="username" placeholder="please enter your name"></label></form>',

    'basic callback',
    h `[p [::call ${$p => $p.attr('id', 'p-a-id')}] "a p"]`,
    '<p id="p-a-id">a p</p>',

    'class list is handle',
    h `[p [:class [a b c]] abc]`,
    '<p class="a b c">abc</p>',

    'class string is handle',
    h `[p [:class "foo bar"] a-para]`,
    '<p class="foo bar">a-para</p>',

    'style element',
    h `[style
        [[ul, ol]
         margin-left 2em
         color gray]
        [body
         background black
         color white]
        [[header>p, footer p]
         font-size smaller
         color black]]`.trim(),
    `<style>
ul, ol {
  margin-left: 2em;
  color: gray;
}
body {
  background: black;
  color: white;
}
header>p, footer p {
  font-size: smaller;
  color: black;
}
</style>`,

    'style element with inline raw css',
    h `
[style
 ["p q" color #333]
 [[p code] color red
           border [1px solid]
           --some-var [200% solid !important]
           background "#EEE !important"]
 "@layer some-layer" {
   [":root" --some-var 300%
            font-family "consolas, monospace"]
   [p color black]
 }
 "/* a comment */"
 [blockquote border-left [0.6em solid gray]]]`,
    `<style>
p q {
  color: #333;
}
p code {
  color: red;
  border: 1px solid;
  --some-var: 200% solid !important;
  background: #EEE !important;
}
@layer some-layer{:root {
  --some-var: 300%;
  font-family: consolas, monospace;
}
p {
  color: black;
}
}/* a comment */blockquote {
  border-left: 0.6em solid gray;
}
</style>`,

    'style property',
    h `[div [:style [background black color white border [solid 1px yellow]]]
            "white text"]`,
    '<div style="background: black; color: white; border: solid 1px yellow;">\
white text</div>',

    'html injection',
    h `[p <p>]`,
    `<p>&lt;p&gt;</p>`,

    'class array',
    h `[div [:class [a b]] ab-div]`,
    '<div class="a b">ab-div</div>',

    'top level node should work',
    h `[html [:lang en] root] [body [:id top] bdy]
       [head [link [:rel stylesheet href style.css]]
             [script [:src index.js]]]`,
    `<html lang="en">root</html><body id="top">bdy</body>` +
        `<head><link rel="stylesheet" href="style.css">` +
        `<script src="index.js"></script></head>`,

    'top level doctype basic work',
    h `[doctype html]`,
    `<!DOCTYPE html>`,

    'escape quote in string',
    h `[p "double: double [\"] single [\'] back quote \` back slash \\"] [p 'single: double [\"] single [\'] back quote \` back slash \\']`,
    `<p>double: double ["] single ['] back quote \` back slash \\</p>\
<p>single: double ["] single ['] back quote \` back slash \\</p>`,

    'p toplevel list',
    h `[p text] [p text2]`,
    '<p>text</p><p>text2</p>'
)

tsjson.domTool['macro:style::syntax'] = (css, k, v) => `"${v}"`
'macro:style:property'
tcj.defineEqTask('style property macro',
h `[style [[@property --textbox-height]
           syntax <length>
           inherits true
           initial-value auto]]`,
`<style>
@property --textbox-height {
  syntax: "<length>";
  inherits: true;
  initial-value: auto;
}
</style>`)
tcj.runClass().exit()
