#!/usr/bin/env node
import tsj from '../index.esm.js'
import esMain from 'es-main'
import process from 'process'

let strf = (x, o = option) => {
    if (o.json || !stro) {
        return JSON.stringify(x, null, '  ')
    }
    return stro(x, {
        indent: '  ', inlineCharacterLimit: 40
    })
}
let stro = null

export function trimChar(s, c) {
    if (s.charAt(0) == c) s = s.trim().slice(1, -1).trim()
    return s
}
export let getInputPromise = null
export let getInputString = null
export function getInput(p = process) {
    if (getInputPromise) return getInputPromise
    const opt = argvParse(p.argv.slice(2))
    if (!opt.input) getInputPromise = new Promise(ok => {
        p.stdin.on('data', c => opt.input += c)
        p.stdin.on('end', () => ok(opt))
    })
    else getInputPromise = Promise.resolve(opt)
    return getInputPromise.then(opt => {
        getInputString = opt.input
        return opt
    })
}

var option
function argvParse(a) {
    const o = {argv: a, input: ''}
    if (a[0] == '--json') {
        o.json = true
        a.shift()
    }
    if (a.length > 0) o.input = a.splice(0).join(' ')
    return o
}

if (esMain(import.meta)) {
    const stroLoad = import('stringify-object')
          .then(x => stro = x.default)
          .catch(e => {})
    stroLoad.then(() => getInput()).then(opt => {
        option = opt
        const s = trimChar(opt.input, '`')
        let json = null
        let parseError
        try {json = eval('tsj.j `' + s + '`')}
        catch (err1) {parseError = err1}
        if (json) return console.log(strf(json))
        tsj.bracket = '[ ]'.split(' ')
        try {json = eval('tsj.j `' + s + '`')}
        catch (err2) {throw parseError}
        console.log(strf(json))
    }).catch(e => {
        console.error(e)
        process.exit(1)
    })
}
