#!/usr/bin/env node
import fs from 'fs'
import process from 'process'
import {PassThrough} from 'stream'

function isMain(meta = import.meta, p = process) {
    return meta.url.replace(/^file:../, '') == p.argv[1]
}

function build(opt) {
    const {file, name} = opt
    const read = fs.createReadStream(file, 'utf8')

    buildCommonJs({file, name, stream: copyStream(read)})
    buildEsModule({file, name, stream: copyStream(read)})
    buildBrowser({file, name, stream: copyStream(read)})
}

function copyStream(str) {
    const p = new PassThrough()
    str.pipe(p)
    return p
}

function buildCommonJs({file, name, stream}) {
    const fout = file.replace(/\.js$/, '.cjs')
    const out = fs.createWriteStream(fout, 'utf8')
    wrapWrite(['', ''], stream, out)
}

function buildEsModule({file, name, stream}) {
    const fout = addMidSuffix(file, 'esm')
    const out = fs.createWriteStream(fout, 'utf8')
    wrapWrite(
        `const exports = {}; export default exports; {\n}`.split('\n'),
        stream, out)
}

function buildBrowser({file, name, stream}) {
    const fout = addMidSuffix(file, 'browser')
    const out = fs.createWriteStream(fout, 'utf8')
    wrapWrite(
        `var ${name} = {}; { const exports = ${name};\n}`.split('\n'),
        stream, out)
}

function addMidSuffix(file, mid) {
    return file.replace(/\..{1,5}?$/, `.${mid}$&`)
}

function wrapWrite(wrap, stream, out) {
    out.write(wrap[0])
    stream.pipe(out, {end: false})
    stream.on('end', () => out.end(wrap[1]))
}

function main(argv) {
    while (argv.length > 0) {
        const [file, name] = argv.splice(0, 2)
        if (!name) throw new Error(`no global name provide for ${file}`)
        build({file, name})
    }
}

if (isMain(import.meta)) {
    main(process.argv.slice(2))
}
