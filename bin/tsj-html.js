#!/usr/bin/env node
import tsj from '../index.esm.js'
import {domTool as cht} from '../lib/cheerio-dom-tool.js'
import * as cheerio from 'cheerio'
import esMain from 'es-main'

const domTool = Object.assign(tsj.domTool, cht)
domTool.setCheerioModule(cheerio)

var tsjson = tsj
tsj.htmlAndPrint = function (...args) {
    const ret = this.html(...args)
    return this.htmlAndPrintDomPrint(ret)
}
tsj.htmlAndPrintDomPrint = function (dom) {
    let h = domTool.toHtml(dom)
    if (h.slice(0, 5) == '<css>') {
        h = h.slice(5, -6)
            .replace(/&gt;/g, '>')
            .replace(/&lt;/g, '<')
            .replace(/&amp;/g, '&')
    }
    console.log(h)
    return h
}
export default tsj

import {getInput, trimChar} from './tsj.js'
if (esMain(import.meta)) {
    getInput().then(opt => {
        let s = opt.input
        s = trimChar(s, '`')

        const c = s.charAt(0)
        let wrap = ['tsj.html `', '`']
        if (c == '[') tsj.bracket = '[ ]'.split(' ')
        else if (c == '(') true
        else wrap = ['', '']

        const $e = eval(wrap.join(s))
        tsj.htmlAndPrintDomPrint($e)
    }).catch(e => {
        console.error(e)
        process.exit(1)
    })
}

