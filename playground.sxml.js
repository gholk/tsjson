`[doctype html]
[html
 [body
  [h1 "tsjson play ground"]
  [macro checkbox ${l => {
    const t = tsj.domTool
    l[0] = 'label'
    let d = {}
    if (t.isAttributeDict(l[1])) [d] = l.splice(1, 1)
    d.type = 'checkbox'
    const input = ['input', d]
    const [name] = l.splice(1, 1, input)
    d.name = name
    if (!l[2]) l[2] = name.replace(/[-_]/g, ' ')
  }}]
  [macro radio ${l => {
    const t = tsj.domTool
    let i = 1
    if (t.isAttributeDict(l[i])) i++
    const group = l.splice(i, 1)
    t['macro:checkbox'](l)
    const d = l[1][1]
    d.type = 'radio'
    d.value = d.name
    d.name = group
  }}]

  [style
   ["textarea:not([hidden])"
    display block
    width 100%
    height 8em]
   [textarea.sexp height 15em]
   [.warn color red]
   [[* + button] margin-left 0.5em]]

  [textarea [:class sexp :name sexp]]
  [textarea [:hidden '' :class [example sxml]] "
(input (:placeholder username))
(button (:onclick \${e => {
  const b = e.target
  let s = \`hey \${b.previousSibling.value || 'unnamed'}!\`
  if (!b.nextElementSibling.firstChild.checked) s += '\\nplease enable cookie'
  alert(s)}}
         :style (color red background yellow
                 margin 0.5em
                 border (solid 1px black)))
 greet)
"
[do ${() =>
  '(macro checkbox ${' + tsj.domTool['macro:checkbox'].toString() + '})'
}]
"
(checkbox accept-cookie)
"]
  [textarea [:hidden '' :class [example tabular]]
"(name id summary)
book1 1 'the first book'
book2 2 'another book'"]
  [textarea [:hidden '' :class [example json]]
'1 2 a b ("str1" "str2") (:ok false :val null :re \${/[Yy]es/})
\${x => x*2} @\${[1, 2, {aaa: function () { return "a"}}]}']

  [button [:onclick "ui.transform()"] transform]
  [button [:onclick "ui.loadExample()"] example]
  [radio input-type json 'json mode']
  [radio [:checked ''] input-type sxml 'sxml mode']
  [radio input-type tabular 'unzip object table']
  [label [input [:type checkbox :name square-bracket]]
         "square bracket"]
  [checkbox [:checked ''] append-html "append html directly"]
  [button [:onclick "stateJsLoad(this)"] "load state"]
  [output [textarea '<input placeholder="username"><button>greet</button><label><input type="checkbox" name="accept-cookie">accept cookie</label>']
          [div]]
  [script [:src lib/macro-more.browser.js]]
  [script [:src index.browser.js]]
  [script [:src stringify-object.browser.js]]
  [script ${() => {
   var tsj = tsjson
   Object.assign(tsjson.domTool, macroMore.domTool)
   const $ = (e) => {
     return document.querySelector(e)
   }
   function stringify(x) {
     if (typeof stringifyObject == 'function') {
       return stringifyObject(x, {
         indent: '  ',
         inlineCharacterLimit: 40
       })
     }
     return JSON.stringify(x, null, '  ')
   }
   const ui = {
     transform(option) {
       if (!option) option = this.getOption()
       let m = 'j'
       if (option.type == 'sxml') m = 'html'
       if (option.type == 'tabular') m = 'jtable'
       const source = $('textarea.sexp').value
       tsjson.bracket = option.bracket
       const res = eval(`tsjson.${m} \`${source}\``)
       $('output textarea').value = this.xToString(res)
       if (option.type == 'sxml' && option.append) {
         const div = $('output div')
         div.textContent = ''
         div.appendChild(res)
       }
     },
     xToString(x) {
       const {domTool} = tsjson
       if (!domTool.isNode(x)) return stringify(x)
       const div = domTool.createNode('div')
       domTool.append(div, x.cloneNode(true))
       return div.innerHTML
     },
     getOption() {
       const o = {}
       o.type = $('[name=input-type]:checked').value
       const sq = $('[name=square-bracket]').checked
       o.bracket = (sq ? '[ ]' : '( )').split(' ')
       o.append = $('[name=append-html]').checked
       return o
     },
     loadExample() {
       const o = this.getOption()
       $('textarea.sexp').value = $(`.example.${o.type}`).value
     },
     init() {
       this.loadExample()
       if (typeof stringifyObject == 'undefined') {
         $('output').appendChild(tsjson.html `
(p (:class warn) "
npm package stringify-object (stringify-object.browser.js) not found;
stringify with JSON.stringify; non-json value will not stringify.")`)
       }
     }
   }
   ui.init()

   function stateJsLoad(e) {
     tsj.html `(${$('body')} (script (:src lib/state.js ::id js)))`
     tsj.domTool.context.js.onload = () => patchTsjson(tsj)
     if (e) e.disabled = true
   }
  }}]
]]
`
