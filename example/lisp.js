#!/usr/bin/env node

/** usage

cli: node example/lisp.js [progn 1 2 3]
cli: node example/lisp.js '(progn 1 2 3)'

example:

    (map (lambda (x) (fact x))
         (quote (1 2 3 4 5 6 7 8)))


todo:

  - fix function get call problem, should function self evaluate?
  - better access tsj.tag log sexp`` in js function
  - support quote syntax sugar
  - should macro run in current scope?

 */

//** esm *******************
import tsj from '../index.esm.js'
import process from 'process'
import esMain from 'es-main'
import {log as m, exp as ttlEx, ttLogger} from '@gholk/tt-logger'
{
    const d = process.env.DEBUG
    if (d && d.length > 1) ttLogger.include `${d}`
}
//****************/

/** browser *******************
var tsj = tsjson
var m = ttl.log, ttlEx = ttl.exp, ttLogger = ttl.ttLogger
var isMain = x=>false
//***************/

ttLogger.exclude `is-literal native eval-form function-new`
ttLogger.exclude `eval-start scope-start sexp-tag lisp-set macro-expand`
ttLogger.exclude `scope-add`

class Tago {
    constructor(t,x) {
        this.type = t
        this.value = x
    }
    static xToString(x) {
        return `#(${x.type} <${x.value}>)`
    }
    toString() {
        return this.constructor.xToString(this)
    }
}
tsj.tag = (t, x) => new Tago(t, x)

const testp = {
    isSym: (e) => typeof e == 'string',
    isLiteral: e => typeof e == 'number' || e.type == 'string',
    // TODO is special form literal? (lambda js macro-lambda macro-js)
    isSf: e => {
        const x = testp
        if (!x.isList(e)) return false
        const t = e[0]
        return x.isNative(t) || x.isMacro(t) || x.isLambda(t)
    },
    isList: e => Array.isArray(e),
    isNative: t => t.slice(-2) == 'js',
    isMacro: t => t.slice(0, 5) == 'macro',
    isLambda: t => t.slice(-6) == 'lambda',
    isQuote: t => t == 'quote'
}

{
    const f0 = ttlEx.xToString.bind(ttlEx)
    let nestLevel = 0
    const f1 = ttlEx.xToString = function (x) {
        if (testp.isList(x)) {
            nestLevel++
            const r = '(' + x.map(f1).join(' ') + ')'
            nestLevel--
            return r
        }
        if (nestLevel > 0 && typeof x == 'function') {
            return `#(function-js <${String(x)}>)`
        }
        return f0(x)
    }
}

function lex(...arg) {
    const ss = new tsj.SexpStream(tsj.optionJsexp)
    ss.tag(...arg)
    if (ss._remain.length > 0) {
        throw new Error(`unexpected end of data: ${ss._remain}`)
    }
    return ss
}
function parse(ss) {
    const {Stack, Queue, token} = tsj
    const l = ss.result
    const xs = new Queue(ss.slotVar)
    const s = new Stack()
    s.push([])
    for (const x of l) {
        switch (x.type) {
        case 'open': {
            const c = []
            s.push(c)
            break
        }
        case 'close': {
            const l = s.pop()
            s.top.push(l)
            break
        }
        default: {
            let r = x
            if (x.type == 'variable-list-future') {
                r = x.resolve(xs.getN(x.length))
            }
            else if (x.type == 'variable-future') {
                r = wrap(xs.get())
            }
            if (r.type == 'number' || r.type == 'symbol') s.top.push(r.value)
            else s.top.push(r)
            break
        }
        }
    }
    return s.top
}
function sexp(...arg) {
    return parse(lex(...arg))[0]
}
const l = sexp
const s = sexp
m `sexp-tag ${sexp `(a b)`}`

function evall(e, s) {
    const {isSym, isList, isLiteral, isNative, isMacro, isLambda, isQuote} = testp
    m `is-literal ${isLiteral(e)}`
    if (isSym(e)) {
        if (e in s) return s[e]
        throw new Error(`symbol ${e} undefined`)
    }
    if (isLiteral(e)) return e
    if (isList(e)) {
        m `eval-start ${e}`
        let [fn, ...arg] = e
        let [type, ...f] = evall(fn, s)
        m `eval-form ${type} ${f}`
        if (isQuote(type)) return arg[0]
        if (!isMacro(type)) arg = arg.map(x => evall(x, s))
        let r
        if (isLambda(type)) r = applyl(f, arg, s)
        else if (isNative(type)) {
            m `native ${String(f)} ${String(arg)}`
            r = wrapPrimitive(f[0](s, ...arg))
        }
        if (isMacro(type)) {
            m `macro-expand ${r}`
            r = evall(r, s)
        }
        return r
    }
    m `evall-fail ${e}`
    throw new Error(`unknown expr ${e}`)
}

function applyl(fn, arg, sp) {
    const [argn, ...body] = fn
    m `scope-start ${argn}`
    const s = Object.create(sp)
    for (let i=0; i<argn.length; i++) {
        m `scope-add ${argn[i]} ${arg[i]}`
        if (argn[i] == '.') {
            s[argn[i+1]] = arg.slice(i)
            break
        }
        s[argn[i]] = arg[i]
    }
    return body.map(exp => evall(exp, s))[body.length-1]
}

const topScope = {
    __tsj__: tsj,
    __ttlogger__: ttLogger,
    get __scope__() { return this },
    eval: ['js', (s, e) => evall(e, s)],
    apply: ['js', (s, f, l) => evall([f, ...l].map(x => ['quote',x]), s)],
    cons: [`js`, (s, a, b) => [a].concat(b)],
    car: [l`js`, (s, x) => x[0]],
    cdr: [l`js`, (s, x) => x.slice(1)],
    nil: 'nil',
    t: 't',
    'if': [l`macro-js`, (s, t, a, b) => {
        const p = evall(t, s)
        const {nil} = s
        if (p != nil) return a
        return b || nil
    }],
    'set!': ['js', (s, k, v) => {
        while (s) {
            if (s.hasOwnProperty(k)) {
                let o = s[k]
                s[k] = v
                return o
            }
            s = Object.getPrototypeOf(s)
        }
        throw new Error(`undefined variable ${k}`)
    }],
    'set': [`js`, (s, k, v) => { s[k] = v }],
    'progn': ['js', (s, ...arg) => arg[arg.length-1]],
    'eq?': [`js`, (s, a, b) =>
        (a == b) ? s.t : s.nil],
    quote: [`quote`],
    // (js '(a b) "a+b" a b)
    js: [`js`, (s, an, code, ...arg) => {
        let c = code.value.trim()
        if (c.charAt(0) != ';' && c.slice(-1) != ';') c = 'return ' + c
        const f = new Function(['__scope__,tsj'].concat(an).join(','), c)
        m `function-new ${String(f)} ${arg}`
        return f(s, tsj, ...arg)
    }]
}

function unwrap(x) {
    if (x && x.type == 'string') return x.value
    return x
    const {isList, isLiteral, isSym} = testp
    if (isLiteral(x)) return x.value
    if (isSym(x)) return "'" + x.value
    if (isList(x)) return x.map(x => unwrap(x))
    return x
}
function wrap(x) {
    if (x && typeof x == 'object') return x
    if (typeof x == 'string') return tsj.tag('string', x)
    if (typeof x == 'number') return tsj.tag('number', x)
    if (typeof x == 'boolean') return x ? l`t` : l`nil`
    if (x == null) return l`nil`
    return x
}
function wrapPrimitive(x) {
    switch (typeof x) {
    case 'symbol':
        return x.description

    case 'string':
    case 'number':
        break

    case 'object':
        if (x) break

    case 'boolean':
    case 'undefined':
        x = x ? 't' : 'nil'
    }
    return x
}


// Object.assign(global, {wrap, unwrap})

function evalSexp(s) {
    return evall(s, Object.create(topScope))
}
function main(x) {
    const s = x || process.argv.slice(2).join(' ')
    if (s.trim().charAt(0) == ';') {
        return m `eval-js ${eval(s)}`
    }
    if (s.trim().charAt(0) == '[') tsj.bracket = '[ ]'.split(' ')
    const tk = lex([s])
    const ar = parse(tk)

    let r = evalSexp(['progn'].concat(basicFunctionSexp).concat(ar))
    m `eval-result ${r}`
    return r
}

var basicFunctionSexp = l`(
 (set (quote list) (quote (lambda (. l) l)))
 (set (quote macro-lambda)
      (quote (macro-lambda (arg body)
              (list (quote quote)
                    (list (quote macro-lambda)
                          arg body)))))
 (set (quote list?)
      (quote (lambda (x) (js (quote (x)) "Array.isArray(x)" x))))
 (set (quote define)
      (macro-lambda (k v)
       (progn
        (if (list? k)
            (progn (define fname (car k))
                   (define args (cdr k))
                   (define k fname)
                   (define v (list (quote lambda) args v))) 1)
        (list (quote set) (list (quote quote) k) v))))
 (define lambda
  (macro-lambda (arg body)
   (list (quote quote)
         (list (quote lambda) arg body))))
 (define = eq?)
(define (append a b)
 (js (quote (a b)) "a.concat(b)" a b))
(define index-of (lambda (l x)
 (js (quote (l x)) "; let r = l.indexOf(x); if (r==-1) r = 'nil'; return r"
  l x)))
(define lambda-js (macro-lambda (arg body)
    (list (quote lambda) arg
          (append (list (quote js) (list (quote quote) arg) body)
                  arg))))
(define slice (lambda (l a b)
 (js (quote (l a b)) "l.slice(a,b)" l a b)))
 (define length (lambda-js (l) "l.length"))
 (define map (lambda (f l)
  (if (null? l) l (cons (f (car l)) (map f (cdr l))))))
 (define null? (lambda (l) (= 0 (length l))))
 (define fact (lambda (n) (if (= n 0) 1 (* n (fact (- n 1))))))
 (define string= (lambda-js (a b) "a.value == b.value ? 't' : 'nil'"))
 (define debug (lambda-js () ";debugger; return 'debug-exit'"))
 (define define-js
  (macro-lambda (fa body)
   (progn
    (define fn (car fa))
    (define arg (cdr fa))
    (list (quote define) fn (list (quote lambda-js) arg body)))))
 (define-js (symbol s) "s.value")
 (define-js (symbol-string o) "tsj.tag('string',o)")
 (define-js (char-at s n) "tsj.tag('string', s.value.charAt(n))")
 (define define-js-op2
  (macro-lambda (op)
   (progn '"\(define ,op (lambda-js (a b) ,(concat "a" (symbol-string op) "b")))"'
          (list (quote define) op
                (list (quote lambda-js) (quote (a b))
                      (concat "a" (symbol-string op) "b"))))))
(define-js (nth l n) "l[n]")
(define lambda-js (macro-lambda (arg body)
  (progn
   (define dot-pos (index-of arg (quote .)))
   (if (eq? dot-pos nil)
    (define js-arg arg)
    (define js-arg (append (slice arg 0 dot-pos)
                           (list (nth arg (+ 1 dot-pos))))))
    (list (quote lambda) arg
          (append (list (quote js) (list (quote quote) js-arg) body)
                  js-arg)))))
 (define concat (lambda (. l)
  (js (quote (l)) "tsj.tag('string', l.map(x=>x.value || x).join(''))" l)))
((macro-lambda (opl)
  (cons (quote progn)
        (map (lambda (op)
              (list (quote define-js-op2) op))
             opl)))
 (+ - * / ** | & < <= > >= ^))
 (define (pair a b) (list a b))
 (define (pair-left ab) (car ab))
 (define (pair-right ab) (car (cdr ab)))
 (define (second l) (car (cdr l)))
 (define first car)
 (define (third l) (nth l 2))
 (define let (macro-lambda (vars body)
  (progn
   (define names (map car vars))
   (define exprs (map (lambda (l) (second l)) vars))
   "\((lambda ,names ,body) ,@vars)"
   (cons (list (quote lambda) names body)
         exprs))))
(define let* (macro-lambda (vars body)
 (if (null? vars) (list (quote let) (list) body)
     (let ((var (car vars)))
      (list (quote let) (list (list (car var) (second var)))
            (list (quote let*) (cdr vars) body))))))
(define define-syntax (macro-lambda (fa body)
 (let ((fn (car fa))
       (arg (cdr fa)))
  (list (quote define) fn
        (list (quote macro-lambda) arg body)))))
(define-syntax (and a b)
 (list (quote let) (list (list (quote ar) a))
  (append (quote (if ar)) (cons b (quote (ar))))))
(define-js (list? x) "Array.isArray(x)")
(define (quasi-quote-fn e uq)
 (if (list? e)
  (if (and (= (length e) 2) (eq? (car e) uq))
   (second e)
   (cons (quote list)
         (map (lambda (x) (quasi-quote-fn x uq))
              e)))
  (list (quote quote) e)))
(define-syntax (quasi-quote e)
 (quasi-quote-fn e (quote unquote)))
(define-syntax (qq e)
 (quasi-quote-fn e (quote uq)))
(define (string-out f)
 (qq (lambda (. arg) (symbol-string (apply (quote (uq f)) arg)))))
(define (string-in f)
 (qq (lambda (. arg)
  (apply (quote (uq f)) (map (lambda (x) (if (string? x) (symbol x) x))
                     arg)))))
(define (string-io f) (string-in (string-out f)))
(define-js (symbol? x) "typeof x == 'string'")
(define-js (string? x) "x && x.type == 'string'")
(define string-length (string-in length))
(define-syntax (lambda-js arg body)
  (progn
   (define dot-pos (index-of arg (quote .)))
   (if (eq? dot-pos nil)
    (define js-arg arg)
    (define js-arg (append (slice arg 0 dot-pos)
                           (list (nth arg (+ 1 dot-pos))))))
   (if (string= (char-at body 0) ";") t
       (define body (concat "return " body)))
    (qq
     (quote (lambda (uq arg)
             (apply (quote (js (uq (js (quote (arg body))
                                       "new Function('__scope__', arg, body.value)"
                                       js-arg body))))
                    (uq (cons (quote list) js-arg))))))))
(define-js (length l) "l.length")
(define-js (list? x) "Array.isArray(x)")
(define string-slice (string-io slice))
(define-syntax (-> x . fns)
 (if (null? fns)
     x
     (let ((c (car fns))
           (els (cdr fns)))
      (progn (if (list? c)
              (define c (append (list (car c) x) (cdr c)))
              (define c (list c x)))
             (append (list (quote ->) c) els)))))
(define (tree-subst tree a b)
 (let ((find nil)
       (f (lambda (x)
           (if (list? x)
            (let ((ret (tree-subst x a b)))
             (progn (if (car ret) (set! (quote find) t))
                    (second ret)))
            (if (eq? x a)
             (progn (set! (quote find) t) b)
             x)))))
  (let ((t2 (map f tree)))
   (list find t2))))
(define-syntax (--> x . fns)
 (if (null? fns)
     x
     (let ((c (car fns))
           (els (cdr fns)))
      (progn (if (list? c)
                 (let* ((ret (tree-subst c (quote it) x))
                        (find (car ret)))
                       (if find (set! (quote c) (second ret))
                                (set! (quote c) (append c (list x)))))
                 (define c (list c x)))
             (append (list (quote -->) c) els)))))
(define regexp (string-in (lambda-js (x flag) ';
 if (x.charAt(0) == "/") {
   const m = x.match(/^.(.+)\/(\w*?)$/)
   x = m[1]
   flag = m[2]
 }
 return new RegExp(x, flag || "")')))
(define-syntax (regexpq x)
 (if (string? x) (list (quote regexp) x)
                 (qq (regexp (symbol-string (quote (uq x)))))))
(define regexp-match (string-in (lambda-js (s r) ";
  let m = s.match(r)
  m.forEach((x,i,m) => m[i] = tsj.tag('string',x))
  return m")))
(define define? (lambda-js (s) "s in Reflect.getPrototypeOf(__scope__)"))
(define-js (js-call fn . arg)
 "fn(...arg)")
(define log-tag-js (string-in (lambda-js (tag . msg)
 "__scope__.__ttlogger__.log(tag, msg)")))
(define (log-tag tag . msg)
 (progn (if (null? msg) (progn (define msg tag) (define tag (quote lisp-log))))
        (apply log-tag-js (cons tag msg))))
(define-syntax (log tag . msg)
 (if (null? msg) (list (quote log-tag) tag)
                 (append (list (quote log-tag) (qq (quote (uq tag))))
                         msg)))
(define-js (date-now) "Date.now()")
(define-syntax (time-tag-with body)
 (qq (let ((time-delta 0)
           (ret nil))
      (progn (define time-delta (date-now))
             (define ret (uq body))
             (log time-cost (concat (- (date-now) time-delta)
                                    "ms for ")
                            (quote (uq body)))
             ret))))
(define-syntax (while cond . body)
 (let ((f (qq (lambda (f ret)
           (if (eval (uq cond))
               (f f (eval (uq (cons (quote progn) body))))
                    ret)))))
 (qq ((quote (uq f)) (quote (uq f)) nil))))
(define-syntax (or . cond)
 (if (null? cond) nil
 (if (length=1 cond) (first cond)
 (qq (let ((c1 (uq (first cond))))
      (if c1 c1 (uq (cons (quote or) (cdr cond)))))))))
(define-syntax (and . cond)
 (if (null? cond) t
 (if (length=1 cond) (first cond)
 (list (quote let) (list (list (quote c1) (first cond)))
  (list (quote if) (quote c1) (cons (quote and) (cdr cond)) (quote c1))))))
(define (length=1 x) (= (length x) 1))
(define (length=2 x) (= (length x) 2))
(define (prog1 x . elze) x)
)`

if (esMain(import.meta)) main()
