[css

[@layer foo] {
  [.textbox-inside.expand pointer-events auto]
  [.expand height auto]
  [.expand background white]
  [[.textbox-inside .lookup.minimal-only]
    position absolute
    bottom 0
    var [right --textbox-right]
    var [right [--textbox-right] 2px]
    right "var(--textbox-right)"
    left  "var(--textbox-left)"
    pointer-events auto]
}

[block @layer foo]
  [.textbox-inside.expand pointer-events auto]
  [.expand height auto]
  [[.textbox-inside .lookup.minimal-only]
    position absolute
    bottom 0
    left  "var(--textbox-left)"
    pointer-events auto]
[/block]

[block @layer foo]
[block-end]

[@layer foo]
[/layer]

[@layer foo
  [.textbox-inside.expand pointer-events auto]
  [.expand height auto]
  [[.textbox-inside .lookup.minimal-only]
    position absolute
    bottom 0
    left  "var(--textbox-left)"
    pointer-events auto]]

]
