`[do ${()=>{
const tsjson = tsj
const dt = tsjson.domTool
const {isDict, isFunction} = tsjson
dt['macro:template'] = l => {
    l.shift() // template
    const mk = 'macro:' + l[0]
    if (dt[mk]) dt[mk](l)

    if (!isDict(l[1]) && !isFunction(l[1])) l.splice(1, 0, {})
    if (!l[1].class) l[1].class = 'template'
    else l[1].class += ' template'
}
}}]

[template script ${x=>{
if (!dd.main.offline) $('.offline').remove()
else $('.online').remove()
$('html>head').append($('body link'))
$('p:empty').remove()
}}]

[template script [:class offline] ${x=>{
if (!dd.main.offline) $('.offline').remove()
else $('.online').remove()
$('html>head').append($('body link'))
$('p:empty').remove()
}}]

[script [:class template] ${x=>{
if (!dd.main.offline) $('.offline').remove()
else $('.online').remove()
$('html>head').append($('body link'))
$('p:empty').remove()
}}]
`
